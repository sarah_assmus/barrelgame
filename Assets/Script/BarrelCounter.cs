using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelCounter : MonoBehaviour
{
    GameManager manager;

    // Start is called before the first frame update
    void Start()
    {
        manager = FindObjectOfType<GameManager>();  
    }

    private void OnTriggerEnter(Collider other)
    {
        // count success if it's the right color in the right colorbox, 
        //otherwise count failure if wrong color in box 

        if(other.GetComponent<Renderer>().material.color == GetComponent<Renderer>().material.color)
        {
            manager.AddSuccess();
        }
        else
        {
            manager.AddFail();
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
