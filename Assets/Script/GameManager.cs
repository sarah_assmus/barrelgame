using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class GameManager : MonoBehaviour
{
    //reference prefab 
    [SerializeField] GameObject BarrelPrefab;
    // Spawning object every amount of time 
    [SerializeField] float SpawningDelay = 5f;
    // reference the Platform
    [SerializeField] Transform MainPlatform;
    [SerializeField] float minOffset = 0.05f, maxOffset = 0.1f;
    [SerializeField] float plateformWidth, platformHeight, platformDepth;
    [SerializeField] Material[] materials;

    [SerializeField]int successCount, failCount;
    [SerializeField, Range(0, 1)] float badCandies;
    [SerializeField] Material bad;

    [SerializeField] ScoreManager ScoreManager;
    public void AddSuccess()
    {
        successCount++;
    }

    public void AddFail()
    {
        failCount++;
    }


    // Start is called before the first frame update
    void Start()
    {
        // GameObject go = new GameObject()
        plateformWidth = MainPlatform.localScale.x;
        platformHeight = MainPlatform.position.y;
        platformDepth = MainPlatform.position.z;

        //for testing 
        StartSpawnBarrels();
    }


    //how to thatr Spauwning every 5 sec 
    public void StartSpawnBarrels(/*float duration*/) // public so we can call it from some were else 
    {  
        InvokeRepeating("SpawnBarrels", 0, SpawningDelay);
    }

    public void StopSpawningBarrels()
    {
        CancelInvoke("SpawnBarrels");
    }
    public void StartSpawninBarrels()
    {
        InvokeRepeating("SpawnBarrels", 0, SpawningDelay);
    }

    void SpawnBarrels()
    {
        //everytime we spawn the object verryfie the postion on start 
        //instantieate at a specific position 
       
        float x = Random.Range(-plateformWidth/2, plateformWidth/2);
        // OR float x = Random.Range (0f, PlateformWidth) - PlateformWidth / 2); inted of generating -+5 

        //float platfromHeight = MainPlatform.position.y;

        float minY = platformHeight + minOffset;
        float maxY = platformHeight + maxOffset;
        float y = Random.Range(minY, maxY);
        Vector3 position = new Vector3(x, y, platformDepth);
        // Instantiate(BarrelPrefab);
        int _RanMat = Random.Range(0,materials.Length);

        float _badCandies = Random.Range(0f, 1f);

        GameObject _Barrel = Instantiate(BarrelPrefab,position,Quaternion.Euler(Vector3.right * 90f));  /*Quaternion.identity dose not work with the position of our barrel */
        ColliderButton colliderButton = _Barrel.GetComponent<ColliderButton>();
        colliderButton.OnClick.AddListener(ScoreManager.BarrelClick);
        //_Barrel.GetComponent<MeshRenderer>().material = materials[_ranMat];

        if(_badCandies > badCandies)
        {
            _Barrel.GetComponent<Renderer>().material = materials[_RanMat];
        }
        else
        {
            _Barrel.GetComponent<Renderer>().material = bad;
        }
        
        //how to stop spawning or changinf the frequency of an object 



    }

}   

