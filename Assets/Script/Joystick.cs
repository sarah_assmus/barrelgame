using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[System.Serializable] public class FloatEvent :  UnityEvent <float> // class only float
{
       
}
public class Joystick : MonoBehaviour
{
    [SerializeField] Slider joystick;
    [SerializeField] GameObject Plateform;
    public enum SliderState
    {
        left,
        center,
        right
    }

    [SerializeField] SliderState sliderState = SliderState.center;
    //[SerializeField] bool UseSpring;
    [SerializeField] HingeJoint PlateformHingeJoint;
    [SerializeField] bool Dragged;

    public FloatEvent RotationSpeed;


    // Start is called before the first frame update
    void Start()
    {
        // joystick.value = 1;
        // UseSpring = Plateform.GetComponent<HingeJoint>().useSpring;
        PlateformHingeJoint = Plateform.GetComponent<HingeJoint>();
        
    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log(joystick.value);
        /*joystick.value -= Time.deltaTime;
        if (joystick.value<=0)
         {
             joystick.value = 0;
         }*/
        if (Dragged)
        {
            RotationSpeed.Invoke(joystick.value);
        }

        if (sliderState == SliderState.center)
        {
            PlateformHingeJoint.useSpring = true;
            //return;
        }

        if (sliderState == SliderState.right)
        {
            PlateformHingeJoint.useSpring = false;
            joystick.value -= Time.deltaTime;
            if (joystick.value <= 0)
            {
                joystick.value = 0;
                sliderState = SliderState.center;
            }
            
        }

        if (sliderState == SliderState.left)
        {
            //PlateformHingeJoint.useSpring = false;
            joystick.value += Time.deltaTime;
            if (joystick.value >= 0)
            {
                joystick.value = 0;
                sliderState = SliderState.center;
            }
            
        }

        

       /* if (sliderState != SliderState.center)  //other way of doing it by reversing the slider state 
        {
            if (sliderState == SliderState.right)
            {
                joystick.value -= Time.deltaTime;
                if (joystick.value <= 0)
                {
                    joystick.value = 0;
                    sliderState = SliderState.center;
                }
            }

            if (sliderState == SliderState.left)
            {
                joystick.value += Time.deltaTime;
                if (joystick.value >= 0)
                {
                    joystick.value = 0;
                    sliderState = SliderState.center;
                }
            }
        }
        Debug.Log( joystick.value);*/
    }
    public void SliderChange(float value)
    {
        if (value == 0)
        {
            sliderState = SliderState.center;
        }

        else if (value < 0)
        {
            sliderState = SliderState.left;
        }

        else 
        {
            sliderState = SliderState.right;
        }
    }

    public void BeginDrag ()
    {
        // Debug.Log("Hello");
        sliderState = SliderState.center; // dont move slider to center 
        //PlateformHingeJoint.useSpring = false;
        Dragged = true;

    }

    public void PointerUp()
    {
        SliderChange(joystick.value);
        //PlateformHingeJoint.useSpring = true;
        Dragged = false;
    }

    /*public void Select()
    {
        Debug.Log("select");
    }*/



}
