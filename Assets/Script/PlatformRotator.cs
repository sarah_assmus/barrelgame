using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformRotator : MonoBehaviour
{
    [SerializeField] float maxAngle = 35;



    public void RotatePlatform(float rotationSpeed)
    {
        float _rotation = transform.rotation.eulerAngles.z;
        Debug.Log(_rotation);


        //transform.Rotate(Vector3.forward * rotationSpeed);
        if ((_rotation > 0f && _rotation < maxAngle) || (_rotation > (360f - maxAngle)))
        {
            transform.Rotate(-Vector3.forward * rotationSpeed);
        }
        if ((_rotation > maxAngle) && (_rotation < (maxAngle + 10))) 
        {
            transform.eulerAngles = Vector3.forward * maxAngle;
        }
        if ((_rotation > (350 - maxAngle)) && (_rotation < (360f - maxAngle)))
        {
            transform.eulerAngles = Vector3.forward * (360 - maxAngle);
        }


    }
}
//whenever we set a value in the slider, it recieves the call - in this script or the other - and then deactivate this boolean that says use spring when 
//it's at the right or left, and when its in the centre it goes back to normal
//deactivate bool HingeJoint.UseSpring --> turn it to false when the slider is everywhere except for centre state. 