using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using TMPro;
public class ScoreManager : MonoBehaviour
{
    [SerializeField] int score;
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] float explosionScale = 5, explosionDuration = 0.1f, explosionDelay= 0.05f;
    
    public void BarrelClick(ColliderButton colliderButton)

    {
        Color barrelColor = colliderButton.GetComponent<MeshRenderer>().material.color;
        Debug.Log(barrelColor);
        if (barrelColor == Color.black) score++;

        else score--;
        scoreText.text = ("Score: " + score);
        colliderButton.OnClick.RemoveListener(this.BarrelClick);

        Tween.LocalScale(colliderButton.transform, colliderButton.transform.localScale *explosionScale, explosionDuration, 0);
        Destroy(colliderButton.gameObject, explosionDuration+explosionDelay); //explode

    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
